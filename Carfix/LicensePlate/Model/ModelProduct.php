<?php
namespace Carfix\LicensePlate\Model;

use Magento\Framework\Model\AbstractModel;
use Carfix\LicensePlate\Api\Data\ModelProductInterface;

/**
 * Class ModelProduct
 * @method ResourceModel\ModelProduct _getResource()
 * @method ResourceModel\ModelProduct getResource()
 *
 *
 */
class ModelProduct extends AbstractModel implements ModelProductInterface
{
    /**
     * {@inheritdoc}
     */
    protected function _construct()
    {
        $this->_init(\Carfix\LicensePlate\Model\ResourceModel\ModelProduct::class);
    }

    /**
     * {@inheritdoc}
     */
    public function getModelId()
    {
        return $this->getData(self::MODEL_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setProductId($id)
    {
        return $this->setData(self::PRODUCT_ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function setModelId($id)
    {
        return $this->setData(self::MODEL_ID, $id);
    }
}