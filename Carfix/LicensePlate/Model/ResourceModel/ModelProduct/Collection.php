<?php
namespace Carfix\LicensePlate\Model\ResourceModel\ModelProduct;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 *
 */
class Collection extends AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Carfix\LicensePlate\Model\ModelProduct::class, \Carfix\LicensePlate\Model\ResourceModel\ModelProduct::class);
    }
}