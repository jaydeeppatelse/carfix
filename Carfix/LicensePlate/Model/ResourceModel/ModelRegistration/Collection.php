<?php
namespace Carfix\LicensePlate\Model\ResourceModel\ModelRegistration;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 *
 *
 */
class Collection extends AbstractCollection
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Carfix\LicensePlate\Model\ModelRegistration::class, \Carfix\LicensePlate\Model\ResourceModel\ModelRegistration::class);
    }

    /**
     * @param int $modelId
     *
     * @return \Carfix\LicensePlate\Model\ModelRegistration[]
     */
    public function getModelRegistration($modelId)
    {
        $collection = $this->addFieldToFilter(
            'main_table.model_id',
            $modelId
        )->setOrder(
            'id',
            'asc'
        );

        return $collection->getItems();
    }
}