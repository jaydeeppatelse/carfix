var config = {
    map: {
        '*': {
            licensePlateSearch: 'Carfix_LicensePlate/form-mini',
            licensePlateExtendedSearch: 'Carfix_LicensePlate/form-extended'
        }
    }
};