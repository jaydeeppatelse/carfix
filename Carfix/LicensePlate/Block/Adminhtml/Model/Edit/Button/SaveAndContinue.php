<?php
namespace Carfix\LicensePlate\Block\Adminhtml\Model\Edit\Button;

/**
 * Class SaveAndContinue
 *
 *
 */
class SaveAndContinue extends Generic
{
    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save and Continue Edit'),
            'class' => 'save',
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit'],
                ],
            ],
            'sort_order' => 80,
        ];
    }
}
