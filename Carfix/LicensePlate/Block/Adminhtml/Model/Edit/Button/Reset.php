<?php
namespace Carfix\LicensePlate\Block\Adminhtml\Model\Edit\Button;

/**
 * Class Reset
 *
 *
 */
class Reset extends Generic
{
    /**
     * {@inheritdoc}
     */
    public function getButtonData()
    {
        return [
            'label' => __('Reset'),
            'class' => 'reset',
            'on_click' => 'location.reload();',
            'sort_order' => 30
        ];
    }
}
