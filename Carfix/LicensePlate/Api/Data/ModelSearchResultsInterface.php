<?php
namespace Carfix\LicensePlate\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface ModelSearchResultsInterface
 * @api
 *
 *
 */
interface ModelSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get models list.
     *
     * @return ModelInterface[]
     */
    public function getItems();

    /**
     * Set model list.
     *
     * @param ModelInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);
}