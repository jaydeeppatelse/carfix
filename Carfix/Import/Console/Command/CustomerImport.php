<?php
namespace Carfix\Import\Console\Command;

use Carfix\Import\Api\QueueManagementInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\State as AppState;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Indexer\Model\IndexerFactory;
use Carfix\Import\Helper\Data as CarfixHelper;
use Carfix\Import\Model\Import\Customer as CustomerImportModel;

/**
 * Class CustomerImport
 *
 *
 */
class CustomerImport extends AbstractImport
{
    /**
     * @var IndexerFactory
     */
    protected $indexerFactory;

    /**
     * @param AppState $appState
     * @param ObjectManagerInterface $manager
     * @param QueueManagementInterface $queueManagement
     * @param ScopeConfigInterface $scopeConfig
     * @param CarfixHelper $carfixHelper
     * @param IndexerFactory $indexerFactory
     */
    public function __construct(
        AppState $appState,
        ObjectManagerInterface $manager,
        QueueManagementInterface $queueManagement,
        ScopeConfigInterface $scopeConfig,
        CarfixHelper $carfixHelper,
        IndexerFactory $indexerFactory
    ) {
        parent::__construct($appState, $manager, $queueManagement, $scopeConfig, $carfixHelper);

        $this->indexerFactory = $indexerFactory;
    }

    /**
     * @var CustomerImportModel
     */
    protected $customerImport;

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('carfix:import:customers')
            ->setDescription('Run customer import');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_ADMINHTML);
        try {
            $this->customerImport = $this->manager->get(CustomerImportModel::class);
            $this->queueManagement->addReport(CarfixHelper::TYPE_CUSTOMER);
            $this->customerImport->import();
            $this->queueManagement->updateReport($this->customerImport->getFormatedLogTrace(), CarfixHelper::STATUS_SUCCESS);
            $indexer = $this->indexerFactory->create();
            $indexer->load("customer_grid");
            $indexer->reindexAll();
        } catch (\Exception $e) {
            $message = $this->customerImport->getFormatedLogTrace().PHP_EOL.$e->getMessage();
            $this->queueManagement->invalidateReport($message);
            $output->writeln("Error: $message");

            if ($this->scopeConfig->isSetFlag(CarfixHelper::PATH_SEND_EMAIL_ON_FAILURE) &&
                $this->scopeConfig->getValue(CarfixHelper::PATH_EMAIL_TO)
            ) {
                $output->writeln("Sending email...");
                $this->carfixHelper->sendErrorEmail($message);
            }
        }
        $output->writeln("<info>Finished Customer Import</info>");
    }
}