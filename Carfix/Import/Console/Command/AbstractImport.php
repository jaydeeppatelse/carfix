<?php
namespace Carfix\Import\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\State as AppState;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Carfix\Import\Helper\Data as CarfixHelper;
use Carfix\Import\Api\QueueManagementInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\ObjectManagerInterface;

/**
 * Class AbstractImport
 *
 *
 */
abstract class AbstractImport extends Command
{
    /**
     * @var AppState
     */
    protected $appState;

    /**
     * @var QueueManagementInterface
     */
    protected $queueManagement;

    /**
     * Core store config
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var CarfixHelper
     */
    protected $carfixHelper;

    /**
     * @var ObjectManagerInterface
     */
    protected $manager;

    /**
     * @param AppState $appState
     * @param ObjectManagerInterface $manager
     * @param QueueManagementInterface $queueManagement
     * @param ScopeConfigInterface $scopeConfig
     * @param CarfixHelper $carfixHelper
     */
    public function __construct(
        AppState $appState,
        ObjectManagerInterface $manager,
        QueueManagementInterface $queueManagement,
        ScopeConfigInterface $scopeConfig,
        CarfixHelper $carfixHelper
    ) {
        parent::__construct();

        $this->appState = $appState;
        $this->queueManagement = $queueManagement;
        $this->scopeConfig = $scopeConfig;
        $this->carfixHelper = $carfixHelper;
        $this->manager = $manager;
    }
}