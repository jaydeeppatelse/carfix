<?php
namespace Carfix\Import\Console\Command;

use Magento\Framework\App\Area;
use Magento\Framework\App\State as AppState;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Carfix\Import\Helper\Data as CarfixHelper;
use Carfix\Import\Model\Import\Upsell as UpsellImportModel;

/**
 * Class UpsellImport
 *
 *
 */
class UpsellImport extends AbstractImport
{
    /**
     * @var UpsellImportModel
     */
    protected $upsellImport;

    /**
     * Configures the current command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('carfix:import:upsell')
            ->setDescription('Run upsell product import');
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->appState->setAreaCode(Area::AREA_ADMINHTML);
        try {
            $this->upsellImport = $this->manager->get(UpsellImportModel::class);
            $this->queueManagement->addReport(CarfixHelper::TYPE_UPSELL);
            $this->upsellImport->import();
            $this->queueManagement->updateReport($this->upsellImport->getMessage(), CarfixHelper::STATUS_SUCCESS);
        } catch (\Exception $e) {
            $message = $this->upsellImport->getMessage().PHP_EOL.$e->getMessage();
            $this->queueManagement->invalidateReport($message);
            $output->writeln("Error: $message");

            if ($this->scopeConfig->isSetFlag(CarfixHelper::PATH_SEND_EMAIL_ON_FAILURE) &&
                $this->scopeConfig->getValue(CarfixHelper::PATH_EMAIL_TO)
            ) {
                $output->writeln("Sending email...");
                $this->carfixHelper->sendErrorEmail($message);
            }
        }
        $output->writeln("<info>Finished Upsell Product Import</info>");
    }
}