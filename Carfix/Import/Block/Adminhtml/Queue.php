<?php
namespace Carfix\Import\Block\Adminhtml;

use Carfix\Import\Helper\Data as CarfixHelper;

/**
 * Class Queue
 *
 *
 */
class Queue extends \Magento\Backend\Block\Widget\Container
{
    /**
     * Prepare button and grid
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $addButtonProps = [
            'id' => 'run_import',
            'label' => __('Run Import'),
            'title' => __('Run Import'),
            'class' => 'add',
            'button_class' => '',
            'class_name' => 'Magento\Backend\Block\Widget\Button\SplitButton',
            'options' => $this->getAddImportButtonOptions(),
        ];
        $this->buttonList->add('run_import', $addButtonProps);

        return parent::_prepareLayout();
    }

    /**
     * Retrieve options for 'Run Import' split button
     *
     * @return array
     */
    protected function getAddImportButtonOptions()
    {
        $splitButtonOptions = [];

        foreach (CarfixHelper::getAvailableTypes() as $type) {
            $splitButtonOptions[$type] = [
                'label' => __($this->getActionLabel($type)),
                'onclick' => "setLocation('" . $this->getActionUrl($type) . "')",
            ];
        }

        return $splitButtonOptions;
    }

    /**
     * Retrieve import label
     *
     * @param string $type
     *
     * @return string
     */
    protected function getActionLabel($type)
    {
        switch ($type) {
            case CarfixHelper::TYPE_PRODUCT:  return __('Import Products');
            case CarfixHelper::TYPE_CUSTOMER: return __('Import Customers');
            case CarfixHelper::TYPE_PRICE:    return __('Import Prices');
            case CarfixHelper::TYPE_UPSELL:   return __('Import Upsell Products');
            case CarfixHelper::TYPE_CATEGORY: return __('Import Categories');
            case CarfixHelper::TYPE_STOCK:    return __('Import Stock');
        }

        return '';
    }

    /**
     * Retrieve import run url by specified type
     *
     * @param string $type
     *
     * @return string
     */
    protected function getActionUrl($type)
    {
        return $this->getUrl('*/*/import', ['type' => $type]);
    }
}
