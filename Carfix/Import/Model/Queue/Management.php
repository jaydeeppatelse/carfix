<?php
namespace Carfix\Import\Model\Queue;

use Carfix\Import\Api\QueueManagementInterface;
use Carfix\Import\Api\Data\QueueInterface;
use Carfix\Import\Api\QueueRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Carfix\Import\Helper\Data as CarfixHelper;

/**
 * Class Management
 *
 *
 */
class Management implements QueueManagementInterface
{
    /**
     * @var QueueInterface
     */
    protected $queue;

    /**
     * @var QueueRepositoryInterface
     */
    protected $queueRepository;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @param QueueRepositoryInterface $queueRepository
     * @param QueueInterface $queue
     * @param DateTime $date
     */
    public function __construct(
        QueueRepositoryInterface $queueRepository,
        QueueInterface $queue,
        DateTime $date
    ) {
        $this->queueRepository = $queueRepository;
        $this->queue = $queue;
        $this->date = $date;
    }

    /**
     * Add Queue Report
     *
     * @param string $type
     * @param null|string $status
     *
     * @return QueueInterface
     */
    public function addReport($type, $status = null)
    {
        if (!$status) {
            $status = CarfixHelper::STATUS_RUNNING;
        }
        $this->queue->setStatus($status)
            ->setTransactionType($type)
            ->setCreatedAt($this->date->date());
       return  $this->queueRepository->save($this->queue);
    }


    /**
     * Update queue report
     *
     * @param string $updateSummary
     * @param null|string $status
     *
     * @return QueueInterface
     */
    public function updateReport($updateSummary, $status = null)
    {
        if (!$status) {
            $status = CarfixHelper::STATUS_SUCCESS;
        }
        $this->queue->setProcessedAt($this->date->date())
            ->setMessage($updateSummary)
            ->setStatus($status);

       return $this->queueRepository->save($this->queue);
    }

    /**
     * Mark queue report as invalid
     *
     * @param string $updateSummary
     *
     * @return QueueInterface
     */
    public function invalidateReport($updateSummary)
    {
        $this->queue->setProcessedAt($this->date->date())
            ->setMessage($updateSummary)
            ->setStatus(CarfixHelper::STATUS_ERROR);

        return $this->queueRepository->save($this->queue);
    }
}