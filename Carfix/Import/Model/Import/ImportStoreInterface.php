<?php
namespace Carfix\Import\Model\Import;
use Magento\Framework\Exception\LocalizedException;

/**
 * Interface ImportStoreInterface
 *
 *
 */
interface ImportStoreInterface
{
    /**
     * Import data
     *
     * @param string $storeCode
     * @return void
     * @throws LocalizedException
     */
    public function import($storeCode);
}
