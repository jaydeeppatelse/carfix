<?php
namespace Carfix\Import\Model\Import;

use Unirgy\RapidFlow\Helper\Data as RapidFlowHelper;
use Carfix\Import\Helper\Data as CarfixHelper;

/**
 * Class Product
 *
 *
 */
abstract class ImportAbstract
{
    /**
     * @var RapidFlowHelper
     */
    protected $rapidFlowHelper;

    /**
     * @var CarfixHelper
     */
    protected $carfixHelper;

    /**
     * @var string
     */
    protected $message = '';

    /**
     * @param RapidFlowHelper $rapidflowHelper
     * @param CarfixHelper $carfixHelper
     */
    public function __construct(
        RapidFlowHelper $rapidflowHelper,
        CarfixHelper $carfixHelper
    ) {
        $this->rapidFlowHelper = $rapidflowHelper;
        $this->carfixHelper = $carfixHelper;
    }

    /**
     * Add new line to message string
     *
     * @param $message
     *
     * @return void
     */
    protected function addMessage($message)
    {
        $this->message .= $message.PHP_EOL;
    }

    /**
     * Get message string
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}