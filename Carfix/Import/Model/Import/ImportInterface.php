<?php
namespace Carfix\Import\Model\Import;
use Magento\Framework\Exception\LocalizedException;

/**
 * Interface ImportInterface
 *
 *
 */
interface ImportInterface
{
    /**
     * Import data
     *
     * @return void
     * @throws LocalizedException
     */
    public function import();
}
