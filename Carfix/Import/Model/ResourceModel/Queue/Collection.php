<?php
namespace Carfix\Import\Model\ResourceModel\Queue;

use Magento\Framework\Data\AbstractSearchResult;
use Carfix\Import\Api\Data\QueueCollectionInterface;

/**
 * Class Collection
 *
 *
 */
class Collection extends AbstractSearchResult implements QueueCollectionInterface
{
    /**
     * {@inheritdoc}
     */
    protected function init()
    {
        $this->setDataInterfaceName(\Carfix\Import\Api\Data\QueueInterface::class);
    }
}