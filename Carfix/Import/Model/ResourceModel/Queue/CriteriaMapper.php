<?php
namespace Carfix\Import\Model\ResourceModel\Queue;

use Magento\Framework\DB\GenericMapper;

/**
 * Class CriteriaMapper
 *
 *
 */
class CriteriaMapper extends GenericMapper
{
    /**
     * {@inheritdoc}
     */
    protected function init()
    {
        $this->initResource(\Carfix\Import\Model\ResourceModel\Queue::class);
    }
}