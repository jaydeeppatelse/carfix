<?php
namespace Carfix\Import\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Carfix\Import\Api\Data\QueueInterface;

/**
 * Class Queue
 *
 *
 */
class Queue extends AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('queue_manager', QueueInterface::QUEUE_ID);
    }
}