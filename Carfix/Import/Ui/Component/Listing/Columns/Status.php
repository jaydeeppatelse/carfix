<?php
namespace Carfix\Import\Ui\Component\Listing\Columns;

use Magento\Ui\Component\Listing\Columns\Column;
use Carfix\Import\Helper\Data as CarfixHelper;

/**
 * Class Status
 *
 *
 */
class Status extends Column
{
    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $class = '';
                $text = '';
                switch ($item['status']) {
                    case CarfixHelper::STATUS_ERROR:
                        $class = 'grid-severity-critical';
                        $text = __('Error');
                        break;
                    case CarfixHelper::STATUS_SCHEDULED:
                        $class = 'grid-severity-minor';
                        $text = __('Scheduled');
                        break;
                    case CarfixHelper::STATUS_RUNNING:
                        $class = 'grid-severity-minor';
                        $text = __('Running');
                        break;
                    case CarfixHelper::STATUS_SUCCESS:
                        $class = 'grid-severity-notice';
                        $text = __('Success');
                        break;
                }
                $item[$fieldName . '_html'] = "<span class='{$class}'>$text</span>";
            }
        }

        return $dataSource;
    }
}