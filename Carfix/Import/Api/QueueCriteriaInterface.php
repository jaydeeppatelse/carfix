<?php
namespace Carfix\Import\Api;

use Magento\Framework\Api\CriteriaInterface;

/**
 * Interface QueueCriteriaInterface
 *
 *
 */
interface QueueCriteriaInterface extends CriteriaInterface
{
}