<?php
namespace Carfix\Import\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

/**
 * Interface CompanyCollectionInterface
 *
 *
 */
interface QueueCollectionInterface extends SearchResultsInterface
{
    /**
     * Get items
     *
     * @return QueueInterface[]
     */
    public function getItems();

    /**
     * Set items
     *
     * @param QueueInterface[] $items
     *
     * @return $this
     */
    public function setItems(array $items);

}